'use strict';
angular.module('takum')
        .controller('CreateController', function ($scope, Language, Category, Product, $location, messageCenterService) {
            $scope.product = new Product;

            Language.get().$promise.then(function (data) {
                $scope.languages = data.data;
                if ($scope.languages && $scope.languages[0]) {
                    $scope.product.texts = [
                        {language_id: $scope.languages[0].id, name: null, description: null},
                    ];
                }
            });

            Category.get().$promise.then(function (data) {
                $scope.categories = data.data;
                if ($scope.categories && $scope.categories[0])
                    $scope.product.category_id = $scope.categories[0].id;
            });

            $scope.error_msg = '';

            $scope.addProductText = function () {
                $scope.product.texts.push({language_id: $scope.languages[0].id, name: null, description: null});
            };

            $scope.removeProductText = function (idx) {
                if ($scope.product.texts.length > 1)
                    $scope.product.texts.splice(idx, 1);
            };

            $scope.save = function () {
                console.log($scope.product);
                $scope.product.$save().then(function(data){
                    messageCenterService.add('success', 'Product successfully saved.', { status: messageCenterService.status.next, timeout: 5000 });
                    $location.url('/');
                }, function(error){
                    console.log(error);
                    messageCenterService.add('danger', 'Error! product could not be saved.', { status: messageCenterService.status.next, timeout: 5000 });
                });
            };

            $scope.validateField = function (field) {
                return field.$invalid && field.$touched;
            };
        });

