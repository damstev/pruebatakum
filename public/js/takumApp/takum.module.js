'use strict';
// Declare app level module which depends on views, and components
angular.module('takum', ['ngRoute', 'ngResource', 'MessageCenterModule'])
        .config(function ($routeProvider) {
            $routeProvider
                    .when('/', {
                        templateUrl: 'js/takumApp/views/list.html',
                        controller: 'ListController'
                    })
                    .when('/create', {
                        templateUrl: 'js/takumApp/views/create.html',
                        controller: 'CreateController'
                    })
                    .when('/edit/:id', {
                        templateUrl: 'js/takumApp/views/edit.html',
                        controller: 'EditController'
                    })
                    .otherwise({
                        redirectTo: '/'
                    });
        });
