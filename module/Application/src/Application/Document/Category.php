<?php
namespace Application\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="category") */
class Category
{
    /** @ODM\Id */
    private $id;

    /** @ODM\Field(type="string") */
    private $name;
    
    /** @ODM\ReferenceMany(targetDocument="Application\Document\Product") */
    private $products;

    public function __construct() { 
        $this->products = new ArrayCollection();        
    }

    /**
     * @return the $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return the $name
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * @return the $products
     */
    public function getProducts() { 
        return $this->products;         
    }

    /**
     * @param field_type $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @param field_type $name
     */
    public function setName($name) {
        $this->name = $name;
    }    
    
    /**
     * @param Product $product
     */
    public function addProject(Product $product) { 
        $this->products[] = $product;         
    }

}