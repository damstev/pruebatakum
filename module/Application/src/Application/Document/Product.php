<?php
namespace Application\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="product") */
class Product
{
    /** @ODM\Id */
    private $id;
    
    /** @ODM\ReferenceOne(targetDocument="Application\Document\Category") */
    private $category;
    
    /** @ODM\EmbedMany(targetDocument="Application\Document\ProductText") */
    private $productTexts;
    
    public function __construct() { 
        $this->productTexts = new ArrayCollection();        
    }
    
    /**
     * @return the $id
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * @return the $category
     */
    public function getCategory() { 
        return $this->category;         
    }
    
    /**
     * @return the $productTexts
     */
    public function getProductTexts() { 
        return $this->productTexts;         
    }

    /**
     * @param field_type $id
     */
    public function setId($id) {
        $this->id = $id;
    }    
    
    /**
     * @param Category $category
     */
    public function setCategory(Category $category) { 
        $this->category = $category;         
    }
    
    /**
     * @param ProductText $productText
     */
    public function setProductTexts(ProductText $productText) { 
        $this->productTexts[] = $productText;         
    }
    
    /**
     * @param ProductText $productText
     */
    public function resetProductTexts() { 
        $this->productTexts = [];         
    }

}