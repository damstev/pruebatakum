<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class LanguageController extends AbstractRestfulController {

    public function getList() {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $rs = $dm->createQueryBuilder('Application\Document\Language')
                ->getQuery()
                ->execute();

        $finalRs = array();
        foreach ($rs as $row){            
            $finalRs[] = array(
                'id' => $row->getId(),
                'name' => $row->getName()
            );
        }
        
        return new JsonModel(array(
            'data' => $finalRs
        ));
    }

}
