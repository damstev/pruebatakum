<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Application\Document\Product;
use Application\Document\ProductText;

class ProductController extends AbstractRestfulController {

    /**
     * Restfull endpoint to list products
     * @return JsonModel
     */
    public function getList() {


        $category = $this->getRequest()->getQuery('category');

        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $rs = $dm->createQueryBuilder('Application\Document\Product');
        if ($category) {
            $rs = $rs->field('category.$id')->equals(new \MongoId($category));
        }

        $rs = $rs->getQuery()
                ->execute();

        $finalRs = array();
        foreach ($rs as $row) {
            $finalRs[] = $this->serializer($row);
        }

        return new JsonModel(array(
            'data' => $finalRs
        ));
    }

    /**
     * Restfull endpoint to get details of one product
     * @return JsonModel
     */
    public function get($id) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $rs = $dm->find('Application\Document\Product', $id);

//        \Doctrine\Common\Util\Debug::dump($rs);

        return new JsonModel(array(
            'data' => $this->serializer($rs)
        ));
    }

    /**
     * Restfull endpoint to create a new product
     * @param type $data
     * @return JsonModel
     */
    public function create($data) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');

        $product = new Product();

        if (isset($data['category_id']) && $data['category_id']) {
            $category = $dm->find('Application\Document\Category', $data['category_id']);

            if ($category)
                $product->setCategory($category);
        }

        foreach ($data['texts'] as $row) {
            $pt = new ProductText();
            if (isset($row['language_id']) && $row['language_id']) {
                $language = $dm->find('Application\Document\Language', $row['language_id']);
                if ($language)
                    $pt->setLanguage($language);
            }
            $pt->setName($row['name']);
            $pt->setDescription($row['description']);
            $product->setProductTexts($pt);
        }

        $dm->persist($product);
        $dm->flush();

        return new JsonModel(array(
            'data' => $this->serializer($product)
        ));
    }

    /**
     * Restfull endpoint to edit a existing product
     * @param type $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data) {
        $data = $data['data'];
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $product = $dm->find('Application\Document\Product', $id);

        if (isset($data['category_id']) && $data['category_id']) {
            $category = $dm->find('Application\Document\Category', $data['category_id']);

            if ($category)
                $product->setCategory($category);
        }

        $product->resetProductTexts();
        foreach ($data['texts'] as $row) {
            $pt = new ProductText();
            if (isset($row['language_id']) && $row['language_id']) {
                $language = $dm->find('Application\Document\Language', $row['language_id']);
                if ($language)
                    $pt->setLanguage($language);
            }
            $pt->setName($row['name']);
            $pt->setDescription($row['description']);
            $product->setProductTexts($pt);
        }

        $dm->persist($product);
        $dm->flush();

        return new JsonModel(array(
            'data' => $this->serializer($product)
        ));
    }

    /**
     * Restfull endpoint to remove a product
     * @param type $id
     * @return JsonModel
     */
    public function delete($id) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $rs = $dm->find('Application\Document\Product', $id);
        $dm->remove($rs);
        $dm->flush();

        return new JsonModel(array(
            'data' => 'deleted',
        ));
    }

    /**
     * Convert an ODM Entity into a easy to encode php array
     * @param \Application\Document\Product $model
     * @return Array
     */
    private function serializer(\Application\Document\Product $model) {
        $category = $model->getCategory();
        $rs = [
            'id' => $model->getId(),
            'category_id' => ($category) ? $category->getId() : null,
            'categoryName' => ($category) ? $category->getName() : null,
            'texts' => []
        ];

        foreach ($model->getProductTexts() as $pt) {
            $lang = $pt->getLanguage();
            $rs['texts'][] = [
                'language_id' => ($lang) ? $lang->getId() : null,
                'languageName' => ($lang) ? $lang->getName() : null,
                'name' => $pt->getName(),
                'description' => $pt->getDescription()
            ];
        }

        return $rs;
    }

}
