<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Document\User;
use Application\Document\Language;
use Application\Document\Category;
use Application\Document\Product;
use Application\Document\ProductText;

class IndexController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel();
    }
    
    public function loadDataAction() {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        
        /**
         * Languages
         */
        $dm->getSchemaManager()->dropDocumentCollection('Application\Document\Language');
        $enLang = new Language();
        $enLang->setName("English");
        $dm->persist($enLang);
        $frLang = new Language();
        $frLang->setName("French");
        $dm->persist($frLang);
        $esLang = new Language();
        $esLang->setName("Spanish");
        $dm->persist($esLang);
        
        /**
         * Categories
         */
        $dm->getSchemaManager()->dropDocumentCollection('Application\Document\Category');
        $category1 = new Category();
        $category1->setName("Appliances");
        $dm->persist($category1);
        $category2 = new Category();
        $category2->setName("Toys");
        $dm->persist($category2);
        $category3 = new Category();
        $category3->setName("Technology");
        $dm->persist($category3);
        
        /**
         * Products
         */
        $pt1_en = new ProductText();
        $pt1_en->setLanguage($enLang);
        $pt1_en->setName('Stove');
        $pt1_en->setDescription("Se llama estufa al aparato que produce calor y lo emite para calentar uno o varios ambientes. La diferencia fundamental entre el hogar abierto y la estufa tradicional, es que en ésta, el hogar es cerrado, confinando dentro el fuego para proteger a los usuarios de contactos accidentales; además permite un control de la potencia mucho mejor que en el hogar abierto, mediante la regulación del caudal de entrada de aire para la combustión.");
        $pt1_fr = new ProductText();
        $pt1_fr->setLanguage($frLang);
        $pt1_fr->setDescription("Le poêle est un appareil de chauffage rudimentaire. Le plus souvent, il chauffe directement le lieu où il se trouve. Il est rudimentaire dans le sens où il ne comporte pas de circuit caloporteur (circuit de déport thermique). C'est le point principal qui le distingue d'une chaudière. Il propage sa chaleur par convection et par rayonnement.");
        $pt1_fr->setName('Poêle');        
        
        
        $dm->getSchemaManager()->dropDocumentCollection('Application\Document\Product');
        $product1 = new Product();
        $product1->setCategory($category1);
        $product1->setProductTexts($pt1_en);
        $product1->setProductTexts($pt1_fr);
        $dm->persist($product1);
        
//        $model = new Product();
//        $model->setCategory($category3);
//        $dm->persist($model);
        
        $dm->flush();
//        echo 'OK';
        return new ViewModel();
    }

}
